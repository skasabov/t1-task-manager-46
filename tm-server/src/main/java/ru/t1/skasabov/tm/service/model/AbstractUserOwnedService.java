package ru.t1.skasabov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.model.IUserOwnedService;
import ru.t1.skasabov.tm.api.service.model.IUserService;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {

    @NotNull
    protected final IUserService userService;

    protected AbstractUserOwnedService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IUserService userService
    ) {
        super(connectionService);
        this.userService = userService;
    }

}
