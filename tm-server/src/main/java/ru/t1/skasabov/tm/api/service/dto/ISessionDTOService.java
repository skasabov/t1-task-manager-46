package ru.t1.skasabov.tm.api.service.dto;

import ru.t1.skasabov.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

}
