package ru.t1.skasabov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.model.ISessionRepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.model.ISessionService;
import ru.t1.skasabov.tm.api.service.model.IUserService;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.IdEmptyException;
import ru.t1.skasabov.tm.exception.field.IndexIncorrectException;
import ru.t1.skasabov.tm.exception.field.UserIdEmptyException;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService, @NotNull final IUserService userService) {
        super(connectionService, userService);
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<Session> collection) {
        if (collection == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll(collection);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Session> addAll(@Nullable final Collection<Session> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<Session> set(@Nullable final Collection<Session> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.set(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    public Session add(@Nullable final Session model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        @NotNull List<Session> sessions;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            sessions = repository.findAll();
        }
        finally {
            entityManager.close();
        }
        return sessions;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            session = repository.findOneById(id);
        }
        finally {
            entityManager.close();
        }
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable Session session;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            session = repository.findOneByIndex(index);
        }
        finally {
            entityManager.close();
        }
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session removeOne(@Nullable final Session model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session model = findOneById(id);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final Session model = findOneByIndex(index);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOneByIndex(index);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        long size;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            size = repository.getSize();
        }
        finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsSession;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            existsSession = repository.existsById(id);
        }
        finally {
            entityManager.close();
        }
        return existsSession;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsSession;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            existsSession = repository.existsById(userId, id);
        }
        finally {
            entityManager.close();
        }
        return existsSession;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Session> sessions;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            sessions = repository.findAll(userId);
        }
        finally {
            entityManager.close();
        }
        return sessions;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            session = repository.findOneById(userId, id);
        }
        finally {
            entityManager.close();
        }
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable Session session;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            session = repository.findOneByIndex(userId, index);
        }
        finally {
            entityManager.close();
        }
        return session;
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        long size;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            size = repository.getSize(userId);
        }
        finally {
            entityManager.close();
        }
        return size;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session model = findOneById(userId, id);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Session model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOneByIndex(userId, index);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

}
